# Description

Wow! Yesterday I downloaded a movie from my favorite site, 
but it turned out to be a virus! Now the flag on my computer is not the same as before. 
Please cure my computer and send me a flag!
You can download my computer at: [https://mega.nz/#!M5QznaQB!W_3qtr4-9agjRv8I7Fvi91gDObKd6MDtB-bPB7x4yjc](https://mega.nz/#!M5QznaQB!W_3qtr4-9agjRv8I7Fvi91gDObKd6MDtB-bPB7x4yjc)

# Writeup

<details>
<summary>Click to expand</summary>

Суть задания заключается в запуске виртуалки, поиска вируса, после чего
необходимо его деобфусцировать с помощью любого из деобфускаторов
которые можно найти в интернете(т.к. при первой попытке декомпиляции
 будет много мусора). Далее нам достаточно найти алгоритм шифрования
(TripleDes) и определить то,что ключ и вектор инциализации заносятся в 
реестр. Далее достаточно написать обычный дешифровщик файла и
расшифровать pff.txt.

</details>

# Build


# Run


# Deploy


